if (Drupal.jsEnabled) {
    $(function(){
        $('a.fba-obs-link').click(function(){
            $.getJSON(this.href, function(json){
                $('div.observation').prepend(json.behavior);
                $('div.studentid').prepend(json.studentid);
            });
            return false;
        });
    });
}

<?php
if(is_numeric($_GET['nid'])){
?>
  function deleteObs(oid){    
    var xhReq = new XMLHttpRequest();
    xhReq.open("GET", "/fba_obs/delete/"+oid, false);
    xhReq.send(null);    
    e = document.getElementById("observation-id-" + oid);
    e.style.display = "none";
  }
  function addObs(gid){
    e = document.getElementById("addnew-"+gid);
    e1 = e.value;
    
    var dataprovider2 = new AjaxGET();
    dataprovider2 .getData("/fba_obs/save_new/<?php print $_GET['nid']; ?>/"+gid+"/"+e1, function(){
      xml = dataprovider2 .getResponse();
      if(xml == ""){
        alert("Error. Please submit again!");
      }
      x = xml.split("<separator>")
                $(\'div.observation\').prepend(x[1]);
                $(\'div.behavior-group-\'+gid).append(x[2]);
    });
    
  }
  function addObs2(gid, behaviorname){
    var dataprovider2 = new AjaxGET();
    dataprovider2 .getData("/fba_obs/save_new/<?php print $_GET['nid']; ?>/"+gid+"/"+behaviorname, function(){
      xml = dataprovider2 .getResponse();
      if(xml == ""){
        alert("Error. Please submit again!");
      }      
      x = xml.split("<separator>")
                $(\'div.observation\').prepend(x[1]);
    });
    
  }
  function AjaxGET()
  {  
    var xhReq = createXMLHttpRequest();
    var busy = false;
    var responseFunction = null;
    var response = null;
   
    function createXMLHttpRequest( ) {
      try { return new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {}
      try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) {}
      try { return new XMLHttpRequest( ); } catch(e) {}
      alert("XMLHttpRequest not supported");
      return null;
    }
     
    this.getData = function(URL,onResponse)
    {
      if (!busy){
        busy = true;
        xhReq.open("GET", URL, true);
        xhReq.onreadystatechange = respFunction;
        responseFunction = onResponse;
        xhReq.send(null);
      }
    }
     
    this.getResponse = function()
    {
      return response;
    }
     
    function respFunction()
    {
      if (xhReq.readyState == 4 && xhReq.status == 200){
        response = xhReq.responseText;
        busy = false;
        responseFunction();
      }
    }
     
    this.isBusy = function()
    {
      return busy;   
    }  
  }
<?php
}
?>